// ==UserScript==
// @name        improve-startpage
// @include     https://www.startpage.com/*
// @description Hides unwanted stuff and moves ads to the right
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js
// @grant       GM_addStyle
// ==/UserScript==

// Define the waitForElement function
function waitForElement(selector, callback) {
    if ($(selector).length) {
        callback($(selector));
    } else {
        setTimeout(function() {
            waitForElement(selector, callback);
        }, 50); // Check again after 50ms
    }
}

// Move the iframe with id "master-1" to the sidebar
waitForElement("iframe#master-1", function() {
  
	waitForElement("div#sidebar", function() {
		$("iframe#master-1").appendTo("div#sidebar");
    
    // Move sidebar a tiny bit to the right
    $("div#sidebar").css("left", "120px");
	});
});


// TODO: Move Google ads to the sidebar
// ==UserScript==
// @name        improve-feedly
// @include     https://feedly.com/*
// @description Hides unwanted stuff from Feedly
// @require     http://ajax.googleapis.com/ajax/libs/jquery/2.1.0/jquery.min.js
// @grant       GM_addStyle
// ==/UserScript==

// Define the waitForElement function
function waitForElement(selector, callback) {
    if ($(selector).length) {
        callback();
    } else {
        setTimeout(function() {
            waitForElement(selector, callback);
        }, 50); // Check again after 50ms
    }
}

// Hide "Upgrade" button
waitForElement("button span:contains('Upgrade')", function() {
    $("button span:contains('Upgrade')").closest("button").remove();
});

// Hide "Create AI Feed" button
waitForElement('button[aria-label="Create AI Feed"]', function() {
    $('button[aria-label="Create AI Feed"]').remove();
});

// Hide "Create AI Feed" button
waitForElement('button[aria-label="Power Search"]', function() {
    $('button[aria-label="Power Search"]').remove();
});

// Move the button with aria-label "Follow sources" next to the button with aria-label "Hide sidebar"
waitForElement('button[aria-label="Hide sidebar"]', function() {
  $('button[aria-label="Follow sources"]').insertAfter('button[aria-label="Hide sidebar"]');
});

// Hide "Feedly AI" link
waitForElement('button[aria-label="Hide sidebar"]', function() {
	$('div[title="Feedly AI"]').remove();
});

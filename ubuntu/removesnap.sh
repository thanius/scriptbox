#!/bin/bash

# Initialize arrays
ORDER=(snap-store gnome-* gtk-common-themes snapd-desktop-integration bare core* snapd)
REMOVE=()
declare -A UNPRIORITIZED=()

# Get installed snaps
INSTALLED=$(snap list|sed -e 's/ .*//' -e '1d')
readarray -t SNAPS <<< "$INSTALLED"

# Function to check if item matches any pattern in ORDER
matches_order() {
    local item="$1"
    for pattern in "${ORDER[@]}"; do
        if [[ $item == $pattern ]]; then
            return 0 # True, it matches
        fi
    done
    return 1 # False, no match
}

# Separate SNAPS into those that match ORDER and those that don't
for snap in "${SNAPS[@]}"; do
    if matches_order "$snap"; then
        UNPRIORITIZED["$snap"]=1
    else
        REMOVE+=("$snap")
    fi
done

# Add items from ORDER, matching against SNAPS and patterns
for order_item in "${ORDER[@]}"; do
    for snap in "${!UNPRIORITIZED[@]}"; do
        if [[ "$snap" == $order_item || "$snap" == $order_item ]]; then
            REMOVE+=("$snap")
            unset UNPRIORITIZED["$snap"]
        elif [[ $order_item == *"*" ]]; then
            pattern="${order_item/\*/}"
            if [[ "$snap" == $pattern* ]]; then
                REMOVE+=("$snap")
                unset UNPRIORITIZED["$snap"]
            fi
        fi
    done
done

# Remove snaps in order
for snap in ${REMOVE[@]}; do
  sudo snap remove --purge "$snap"
done

# Remove snap daemon and disable snap trigger in apt
# Other wise it will be reinstalled during cache update
sudo apt remove --autoremove snapd -y
echo -e "Package: snapd\nPin: release a=*\nPin-Priority: -10" | sudo tee /etc/apt/preferences.d/nosnap.pref

# Update apt cache
sudo apt update

# Install GNOME suggestions
sudo apt install --install-suggests gnome-software -y

# Reinstall Firefox as deb
sudo add-apt-repository ppa:mozillateam/ppa -y
sudo apt update
sudo apt install -t "o=LP-PPA-mozillateam" firefox -y
echo 'Unattended-Upgrade::Allowed-Origins:: "LP-PPA-mozillateam:${distro_codename}";' | sudo tee /etc/apt/apt.conf.d/51unattended-upgrades-firefox

# Add deb priority over snap for Firefox to avoid getting snap version installed
echo -e "Package: firefox*\nPin: release o=LP-PPA-mozillateam\nPin-Priority: 501" | sudo tee /etc/apt/preferences.d/mozillateamppa

# Profit